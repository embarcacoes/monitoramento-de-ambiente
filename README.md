# Monitoramento de ambiente

Dispositivo para monitoramento de ambientes.

![Greenhouse Monitoring](docs/images/dispositivo_na_case.jpeg)
![Estação Metereologica](docs/images/estacao_metereologica.jpg)

## Desenvolvedores:

Autor: Julio Nunes Avelar.

## Hardware

Principal:

- ESP32S2/ESP32
- BME280
- BH1750
- Thermistor 100k
- Display OLED 0.96" 128x64 I2C
- Case 3D impressa em ABS

Opcional:

- Antena Wifi 2.4GHz externa
- Modulo LoRa SX1276
- Modulo ETHERNET ENC28J60
- Modulo de Carga TP4056
- Bateria 18650 3.7V 2200mAh
- DHT22
- Sensor de Umidade do Solo
- Sensor de Nivel de Água
- Sensor de Chuva
- Sensor de Vento

## Software

- [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/)

## Pré-requisitos:

- [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/).

  Você precisara do ESPIDF para compilar e gravar o Firmware na Placa.

- Uma placa com o [**ESP32**](https://www.espressif.com/en/products/socs/esp32) ou [**ESP32S2**](https://www.espressif.com/en/products/socs/esp32-s2).

  Será necessario uma placa com o **ESP32** ou o **ESP32S2**, essá pode ser uma placa de desenvolvimento ou uma placa profissional.

## Configurações:

## Menu config:

Para gravar o firmware será necessario realizar alguns ajustes no menu config.

Para "invocar" o ESPID, utilize:

```bash
get_idf
```

Após invocar o IDF, nó diretorio, será necessario escolher o MCU, para utilizar o ESP32, utilize:

```bash
idf.py set-target esp32
```

Caso queira utilizar com o ESP32S2, utilize:
Após invocar o IDF, nó diretorio, será necessario escolher o MCU, para utilizar o ESP32s2, utilize:

```bash
idf.py set-target esp32s2
```

Se você estiver usando a extensão do VS-Code, existe uma opção para isso no menu inferior (Elá tem uma caixa com engrenagens como icone e do lado esta escrito esp32 ou esp32s2).

Após escolher o MCU, sera necessario realizar algumas configurações no menu config.

Para acessar o menuconfig utilize:

```bash
idf.py menuconfig
```

ou utilize a opção da extensão do VS-Code (Esta opção também está no menu inferior e tem como icone uma unica engrenagem),

Após acessar menuconfig, configure as seguintes opções:

- Configure a flash com 80mhz.
- Coloque a flash com tamanho minimo de 4 MBs.
- Escolha uma tabela de partições personalizadas.
- Coloque a frequencia da CPU em 240mhz.
- Habilite o https_client.
- Habilite a calibração do ADC.
- Habilite o componente esp_https_server.
- Habilite o display caso queira utilizalo na opção ssd1306.
- Ajuste as configurações do sensor BME280 no menu "BMX280 Options"
- Ajuste as configurações do modulo de comunicação no menu "Configuração do sistema de comunicações"

Após realizar essas configurações, salve e saia.

## Gravando:

Para realizar a gravação primeiro será necessario realizar a compilação do firmware, é possivel compilar utilizando o comando

```bash
idf.py build
```

Após compilar o firmware para gravalo utilize:

```bash
idf.py flash
```

Caso queira utilizar o monitor serial para ver os logs, utilize:

```bash
idf.py monitor
```

Para executar tudo de uma vez, utilize:

```bash
idf.py build flash monitor
```

Esse passo a passo também pode ser realizado pela extensão do ESPIDF para o VS-Code, basta selecionar a opção: **"ESP-IDF build, flash and monitor"**.

## Organização do projeto:

O projeto esta organizado em 5 pastas principais:

- main.

  Pasta com o arquivo principal do programa.

- docs.

  Toda a documentação dos componentes e do projeto estão presentes nessa pasta.

- certs.

  Todos os certificados e chaves utilizadas no projeto estão nesta pasta.

- components.

  Pasta com todos os componentes/modulos para trabalhar com os perifericos e comunicação providas por terceiros ou desenvolvidas como parte do projeto.

- managed_components.

  Pasta com todos os componentes/modulos para trabalhar com os perifericos e comunicação providas pela espressid.

## Duvidas e Bugs:

Em caso de alguma duvida, bug na aplicação ou sujestão, utilize o menu issues do gitlab.

## Licença:

Este repositório está licenciado pela Licença [GNU Affero General Public License V3](https://gitlab.com/embarcacoes/monitoramento-de-ambiente/-/blob/main/LICENSE?ref_type=heads), com exceção dos circuitos e descrições de hardware que estão licenciados pela [Licença CERN Open Hardware License V2 - Strongly Reciprocal](https://gitlab.com/embarcacoes/monitoramento-de-ambiente/-/blob/main/LICENSE_H?ref_type=heads). As bibliotecas de terceiros seguem as licenças utilizadas pelos repositórios originais.

## Bibliotecas de terceiros:

- BMX280: https://github.com/utkumaden/esp-idf-bmx280
- DHT22: https://github.com/Andrey-m/DHT22-lib-for-esp-idf
