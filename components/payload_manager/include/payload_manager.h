/**
 * @file payload_manager.h
 * @author Julio Nunes Avelar (julio@bzoide.dev)
 * @brief
 * @version 0.1
 * @date 2023-09-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef __PAYLOAD_MANAGER__H__
#define __PAYLOAD_MANAGER__H__
#ifdef __cplusplus
extern "C" {
#endif

#include "sdkconfig.h"

#if CONFIG_IDF_TARGET_ESP32 // Se o target for ESP32

#define SDA_GPIO GPIO_NUM_21 // GPIO do SDA
#define SCL_GPIO GPIO_NUM_22 // GPIO do SCL

#elif CONFIG_IDF_TARGET_ESP32S2 // Se o target for ESP32S2

#define SDA_GPIO GPIO_NUM_33 // GPIO do SDA
#define SCL_GPIO GPIO_NUM_34 // GPIO do SCL

#elif CONFIG_IDF_TARGET_ESP32S3 // Se o target for ESP32S2

#define SDA_GPIO GPIO_NUM_33 // GPIO do SDA
#define SCL_GPIO GPIO_NUM_34 // GPIO do SCL

#elif CONFIG_IDF_TARGET_ESP32C3 // Se o target for ESP32S2

#define SDA_GPIO GPIO_NUM_33 // GPIO do SDA
#define SCL_GPIO GPIO_NUM_34 // GPIO do SCL

#endif

// #define ADDR_BH1750 BH1750_ADDR_LO // Endereço I2C padrão do sensor BH1750
// #define ADDR_BMP280 BMP280_I2C_ADDRESS_0 // Endereço I2C padrão do sensor
// BMP280 #define ADDR_BME680 BME680_I2C_ADDR_1
#define DHT_PIN GPIO_NUM_3

#define I2C_MASTER_NUM I2C_NUM_0  /*!< I2C port number for master dev */
#define I2C_MASTER_FREQ_HZ 100000 /*!< I2C master clock frequency */

#define BH1750_CONTINUOS_MODE 1
#define BH1750_ONESHOT_MODE 2
#define BME280_FORCE_MODE 1
#define BME280_CYCLE_MODE 2
#define BME280_SLEEP_MODE 3

/**
 * @brief  Função para inicializar o barramento I2C
 *
 */
void init_i2c();

void stop_i2c();
/**
 * @brief
 *
 */
void init_bh1750();

void delete_bh1750();

void bh1750_set_mode(int mode);

/**
 * @brief
 *
 * @param lux
 */
void read_bh1750(float *lux);

/**
 * @brief
 *
 */
void init_bme280();

/**
 * @brief
 *
 */
void bme280_set_mode(int mode);

void read_bme280(float *temp, float *hum, float *pres);

void init_dht();

void read_dht(double *temperature, double *humidity);

#ifdef __cplusplus
}
#endif
#endif //!__PAYLOAD_MANAGER__H__