#ifndef __UPDATE_H__
#define __UPDATE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "freertos/FreeRTOS.h" // Biblioteca de sistema de tarefas
#include "freertos/task.h"     // Biblioteca de tarefas

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_tls.h"
#include "esp_spiffs.h" // Incluindo a biblioteca de SPIFFS

#include "cJSON.h"
#define HASH_LEN 32

    void init_ota(char *url);

    void get_sha256_of_partitions();

#ifdef __cplusplus
}
#endif

#endif