#include "cJSON.h"
#include "communication.h"
#include "nvs.h"
#include "storage_manager.h"
#include "web_server.h"
#include <stddef.h>
#include <stdlib.h>

void generate_token() {
    mbedtls_sha512_context token_ctx;

    const char *letras =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    u_int8_t max_len = 128;
    unsigned char hash[64];
    char input[129] = {1};

    srand(time(NULL));

    char time[31];
    get_time_str(time);
    get_time(&token_time);

    size_t password_len = 0;

    read_string_size_from_nvs("server_password", &password_len);

    char *password = (char *)malloc(sizeof(char) * password_len + 1);

    read_value_from_nvs("server_user", password, STR);

    sprintf(input, "%s%s", time, password);

    for (size_t i = (strlen(time) + strlen(password)); i < max_len; i++) {
        input[i] = letras[rand() % 61];
    }

    mbedtls_sha512_init(&token_ctx);
    mbedtls_sha512_starts(&token_ctx, 0);

    mbedtls_sha512_update(&token_ctx, (unsigned char *)input, max_len);

    mbedtls_sha512_finish(&token_ctx, hash);

    for (int i = 0; i < 64; i++) {
        sprintf(token + (i * 2), "%02x", hash[i]);
    }

    free(password);
    mbedtls_sha512_free(&token_ctx);
}

bool check_token(char *token_received) {

    if (strcmp(token_received, token) == 0) {
        time_t now;

        get_time(&now);

        double dif = difftime(now, token_time);

        if (dif > 1800) {
            printf("Token expirado\n");
            return false;
        }
        return true;
    }
    return false;
}

esp_err_t handleAPILogin(httpd_req_t *req) {
    int total_len = req->content_len;
    int cur_len = 0;

    char *buf = (char *)malloc(total_len + 1);

    if (total_len >= SCRATCH_BUFSIZE) {
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "content too long");
        return ESP_FAIL;
    }
    while (cur_len < total_len) {
        int received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';

    cJSON *root = cJSON_Parse(buf);

    if (root == NULL) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Failed to parse JSON");
        return ESP_FAIL;
    }

    httpd_resp_set_type(req, "application/json");

    if (!cJSON_HasObjectItem(root, "password") ||
        !cJSON_HasObjectItem(root, "user")) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Missing password or user");

        return ESP_FAIL;
    }

    char *user = cJSON_GetObjectItem(root, "user")->valuestring;
    char *password = cJSON_GetObjectItem(root, "password")->valuestring;

    size_t password_len = 0;
    size_t user_len = 0;

    read_string_size_from_nvs("server_user", &user_len);
    read_string_size_from_nvs("server_passowrd", &password_len);

    char *user_stored = (char *)malloc(sizeof(char) * user_len + 1);
    char *password_stored = (char *)malloc(sizeof(char) * password_len + 1);

    read_value_from_nvs("server_user", user_stored, STR);
    read_value_from_nvs("server_password", password_stored, STR);

    char *buffer = (char *)calloc(1, sizeof(char) * (200));

    if (strcmp(user, user_stored) == 0 &&
        strcmp(password, password_stored) == 0) {
        generate_token();
        sprintf(buffer,
                "{\"token\":\"%s\",\"status\":1,\"message\":\"Usuario Logado "
                "com Sucesso!\"}",
                token);
    } else {
        sprintf(buffer,
                "{\"token\":\"%s\",\"status\":0,\"error\":\"Usuario ou senha "
                "incorretos!\"}",
                token);
    }

    httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

    cJSON_Delete(root);

    if (httpd_req_get_hdr_value_len(req, "Host") == 0) {
        ESP_LOGI(WebServerTAG, "Request headers lost");
    }

    return ESP_OK;
}

esp_err_t handleAPIConfig(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    if (!check_token(received_token)) {
        httpd_resp_set_type(req, "application/json");

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        free(buffer);

        return ESP_OK;
    }

    free(received_token);

    int total_len = req->content_len;
    int cur_len = 0;

    char *buf = (char *)malloc(total_len + 1);

    if (total_len >= SCRATCH_BUFSIZE) {
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "content too long");
        return ESP_FAIL;
    }
    while (cur_len < total_len) {
        int received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';

    cJSON *root = cJSON_Parse(buf);

    if (root == NULL) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Failed to parse JSON");
        return ESP_FAIL;
    }

    httpd_resp_set_type(req, "application/json");

    if (cJSON_HasObjectItem(root, "ap_ssid")) {
        char *ap_ssid = cJSON_GetObjectItem(root, "ap_ssid")->valuestring;

        if (ap_ssid == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("ap_ssid", ap_ssid, STR);
    }

    if (cJSON_HasObjectItem(root, "ap_password")) {
        char *ap_password =
            cJSON_GetObjectItem(root, "ap_password")->valuestring;

        if (ap_password == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("ap_password", ap_password, STR);
    }

    if (cJSON_HasObjectItem(root, "sta_ssid")) {
        char *sta_ssid = cJSON_GetObjectItem(root, "sta_ssid")->valuestring;

        if (sta_ssid == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("wifi_ssid", sta_ssid, STR);
    }

    if (cJSON_HasObjectItem(root, "sta_password")) {
        char *sta_password =
            cJSON_GetObjectItem(root, "sta_password")->valuestring;

        if (sta_password == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("wifi_password", sta_password, STR);
    }

    if (cJSON_HasObjectItem(root, "eap_identify")) {
        char *eap_identify =
            cJSON_GetObjectItem(root, "eap_identify")->valuestring;

        if (eap_identify == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("eap_identify", eap_identify, STR);
    }

    if (cJSON_HasObjectItem(root, "eap_password")) {
        char *eap_password =
            cJSON_GetObjectItem(root, "eap_password")->valuestring;

        if (eap_password == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("eap_password", eap_password, STR);
    }

    if (cJSON_HasObjectItem(root, "mqtt_uri")) {
        char *mqtt_uri = cJSON_GetObjectItem(root, "mqtt_uri")->valuestring;

        if (mqtt_uri == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("mqtt_server_uri", mqtt_uri, STR);
    }

    if (cJSON_HasObjectItem(root, "mqtt_user")) {

        char *mqtt_username =
            cJSON_GetObjectItem(root, "mqtt_user")->valuestring;

        if (mqtt_username == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("mqtt_user", mqtt_username, STR);
    }

    if (cJSON_HasObjectItem(root, "mqtt_password")) {
        char *mqtt_password =
            cJSON_GetObjectItem(root, "mqtt_password")->valuestring;

        if (mqtt_password == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("mqtt_password", mqtt_password, STR);
    }

    if (cJSON_HasObjectItem(root, "mqtt_topic")) {
        char *mqtt_topic = cJSON_GetObjectItem(root, "mqtt_topic")->valuestring;

        if (mqtt_topic == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("mqtt_topic", mqtt_topic, STR);
    }

    if (cJSON_HasObjectItem(root, "update_url")) {
        char *update_url = cJSON_GetObjectItem(root, "update_url")->valuestring;

        if (update_url == NULL) {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to parse JSON");
            return ESP_FAIL;
        }

        write_value_in_nvs("update_url", update_url, STR);
    }

    if (cJSON_HasObjectItem(root, "auto_ap_init")) {
        int8_t auto_ap_init =
            cJSON_GetObjectItem(root, "auto_ap_init")->valueint;
        write_value_in_nvs("auto_ap_init", &auto_ap_init, INT8);
    }

    cJSON_Delete(root);

    httpd_resp_set_type(req, "application/json");

    httpd_resp_send(req, "{\"status\":1}", HTTPD_RESP_USE_STRLEN);

    if (httpd_req_get_hdr_value_len(req, "Host") == 0) {
        ESP_LOGI(WebServerTAG, "Request headers lost");
    }

    return ESP_OK;
}

esp_err_t handleAPIConfigs(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    httpd_resp_set_type(req, "application/json");

    if (!check_token(received_token)) {

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        free(buffer);

        return ESP_OK;
    }

    free(received_token);

    communication_config_t *config = get_config();

    cJSON *root = cJSON_CreateObject(); // precisa fazer

    size_t len;

    cJSON_AddStringToObject(root, "ap_ssid", config->ap_ssid);
    cJSON_AddStringToObject(root, "ap_password", config->ap_password);
    cJSON_AddStringToObject(root, "sta_ssid", config->ssid);
    cJSON_AddStringToObject(root, "sta_password", config->password);
    cJSON_AddStringToObject(root, "mqtt_server_uri", config->mqtt_server_uri);
    cJSON_AddStringToObject(root, "mqtt_user", config->mqtt_user);
    cJSON_AddStringToObject(root, "mqtt_password", config->mqtt_password);
    cJSON_AddStringToObject(root, "mqtt_topic", config->mqtt_topic);
    cJSON_AddStringToObject(root, "eap_identify", config->eap_identify);
    cJSON_AddStringToObject(root, "eap_password", config->eap_password);
    cJSON_AddNumberToObject(root, "auto_ap_init", config->auto_ap_init);

    read_string_size_from_nvs("update_url", &len);

    char *update_url = (char *)malloc(len * sizeof(char));

    read_value_from_nvs("update_url", (void *)update_url, STR);

    cJSON_AddStringToObject(root, "update_url", update_url);

    free(update_url);

    httpd_resp_send(req, cJSON_Print(root), HTTPD_RESP_USE_STRLEN);

    cJSON_Delete(root);

    if (httpd_req_get_hdr_value_len(req, "Host") == 0) {
        ESP_LOGI(WebServerTAG, "Request headers lost");
    }

    return ESP_OK;
}

esp_err_t handleAPIChangeSecrets(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    if (!check_token(received_token)) {
        httpd_resp_set_type(req, "application/json");

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        free(buffer);

        return ESP_OK;
    }

    free(received_token);

    size_t user_name_size = 0;
    size_t password_size = 0;

    read_string_size_from_nvs("server_user", &user_name_size);
    read_string_size_from_nvs("server_password", &password_size);

    char *user_name = (char *)calloc(1, sizeof(char) * (user_name_size + 1));
    char *password = (char *)calloc(1, sizeof(char) * (password_size + 1));

    read_value_from_nvs("server_user", user_name, STR);
    read_value_from_nvs("server_password", password, STR);

    int total_len = req->content_len;
    int cur_len = 0;

    char *buf = (char *)malloc(total_len + 1);

    if (total_len >= SCRATCH_BUFSIZE) {
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "content too long");
        return ESP_FAIL;
    }
    while (cur_len < total_len) {
        int received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                                "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';

    cJSON *root = cJSON_Parse(buf);

    if (!root) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Failed to parse JSON");
        return ESP_FAIL;
    }

    if (cJSON_HasObjectItem(root, "user")) {
        char *user_name_new = cJSON_GetObjectItem(root, "user")->valuestring;

        write_value_in_nvs("server_user", user_name_new, STR);
    }

    httpd_resp_set_type(req, "application/json");

    if (cJSON_HasObjectItem(root, "password")) {
        if (cJSON_HasObjectItem(root, "old_password")) {
            char *password_old =
                cJSON_GetObjectItem(root, "old_password")->valuestring;

            if (strcmp(password_old, password) != 0) {
                httpd_resp_send(
                    req, "{\"status\":0,\"error\":\"Senha antiga incorreta!\"}",
                    HTTPD_RESP_USE_STRLEN);
                return ESP_OK;
            }
        }

        char *password_new = cJSON_GetObjectItem(root, "password")->valuestring;

        write_value_in_nvs("server_password", password_new, STR);
    }

    httpd_resp_send(
        req,
        "{\"status\":1,\"message\":\"Credenciais alteradas com sucesso.\"}",
        HTTPD_RESP_USE_STRLEN);

    if (httpd_req_get_hdr_value_len(req, "Host") == 0) {
        ESP_LOGI(WebServerTAG, "Request headers lost");
    }

    cJSON_Delete(root);

    return ESP_OK;
}
