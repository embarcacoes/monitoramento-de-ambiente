#include "storage_manager.h"
#include "web_server.h"

esp_err_t handleReboot(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    httpd_resp_set_type(req, "application/json");

    if (!check_token(received_token)) {

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        return ESP_OK;
    }

    free(received_token);

    httpd_resp_send(req, "{\"status\":1}", HTTPD_RESP_USE_STRLEN);

    esp_restart(); // Reinicia o sistema

    return ESP_OK;
}

esp_err_t handleQuit(httpd_req_t *req) {
    void generate_token();

    httpd_resp_set_type(req, "application/json");

    httpd_resp_send(
        req, "{\"status\":1,\"message\":\"Usuario deslogado com sucesso.\"}",
        HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

esp_err_t handleInitAPmode(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    httpd_resp_set_type(req, "application/json");

    if (!check_token(received_token)) {

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        return ESP_OK;
    }

    free(received_token);

    httpd_resp_send(req, "{\"status\":1}", HTTPD_RESP_USE_STRLEN);

    write_i8_in_nvs("wifi_mode", 1);

    esp_restart(); // Reinicia o sistema

    return ESP_OK;
}

esp_err_t handleInitSTAmode(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    httpd_resp_set_type(req, "application/json");

    if (!check_token(received_token)) {

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        return ESP_OK;
    }

    free(received_token);

    httpd_resp_send(req, "{\"status\":1}", HTTPD_RESP_USE_STRLEN);

    write_i8_in_nvs("wifi_mode", 0);

    esp_restart(); // Reinicia o sistema

    return ESP_OK;
}

esp_err_t handleInitSTAENTmode(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    httpd_resp_set_type(req, "application/json");

    if (!check_token(received_token)) {

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        return ESP_OK;
    }

    free(received_token);

    httpd_resp_send(req, "{\"status\":1}", HTTPD_RESP_USE_STRLEN);

    write_i8_in_nvs("wifi_mode", 2);

    esp_restart(); // Reinicia o sistema

    return ESP_OK;
}

esp_err_t handleReconfig(httpd_req_t *req) {
    char *received_token = (char *)calloc(1, sizeof(char) * 129);

    size_t token_len = httpd_req_get_hdr_value_len(req, "token");

    if (token_len == 0) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR,
                            "Token não informado");
        return ESP_FAIL;
    }

    httpd_req_get_hdr_value_str(req, "token", received_token, token_len + 1);

    httpd_resp_set_type(req, "application/json");

    if (!check_token(received_token)) {

        char *buffer = (char *)calloc(1, sizeof(char) * (200));

        sprintf(buffer,
                "{\"status\":0,\"error\":\"Token expirado ou invalido!\"}");

        httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);

        return ESP_OK;
    }

    free(received_token);

    httpd_resp_send(req, "{\"status\":1}", HTTPD_RESP_USE_STRLEN);

    restore_default_config(false); // Deleta as configurações do sistema

    esp_restart(); // Reinicia o sistema

    return ESP_OK;
}