#include <stdio.h>
#include <string.h>

#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"
#include "esp_system.h"

#include "communication.h"
#include "payload_manager.h"
#include "storage_manager.h"
#include "time_manager.h"
#include "update.h"
#include "web_server.h"

const char *TAG = "Monitoramento de ambiente";

void dataTask(void *pvParameters);
void callback(char *topic, char *payload, unsigned int topic_lenght,
              unsigned int length);

void app_main(void) {
    ESP_LOGI(TAG, "Iniciando..");
    ESP_LOGI(TAG, "Versão do IDF: %s", esp_get_idf_version());
    ESP_LOGI(TAG, "Memoria livre: %ld bytes",
             esp_get_free_heap_size()); // Log da quantidade de memória livre

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_BASE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    ESP_LOGI(TAG, "Iniciando sistema de arquivos");

    init_storage_manager();

    if (storage_manager_is_first_boot()) {
        restore_default_config(true);
    }

    ESP_LOGI(TAG, "Iniciando comunicação ...");

    config_communication_by_nvs();

    init_wifi();

    ESP_LOGI(TAG, "Memoria livre apos inicialização do wifi: %ld bytes",
             esp_get_free_heap_size()); // Log da quantidade de memória livre

    ESP_LOGI(TAG, "Iniciando sistema de temporização");

    ESP_ERROR_CHECK(init_time_manager());

    ESP_LOGI(TAG, "Temporização iniciada com sucesso");

    vTaskDelay(pdMS_TO_TICKS(100)); // Delay de 100ms

    setcallback(&callback);

    mqtt_start();

    xTaskCreate(dataTask, "payload_task", 8192, NULL, 8, NULL);

    ESP_LOGI(TAG,
             "Iniciando Web Server"); // Log de inicialização do servidor web

    ESP_ERROR_CHECK(init_web_server()); // Inicialização do servidor web

    ESP_LOGI(TAG,
             "Web Server Iniciado"); // Log de inicialização do servidor web

    vTaskDelay(pdMS_TO_TICKS(100)); // Delay de 100ms

    ESP_LOGI(TAG, "Sistema iniciado com sucesso !!!");

    ESP_LOGI(TAG, "Memoria livre apos inicialização do sistema: %ld bytes",
             esp_get_free_heap_size()); // Log da quantidade de memória livre
}

void callback(char *topic, char *payload, unsigned int topic_lenght,
              unsigned int length) {
    ESP_LOGI(TAG, "Mensagem recebida topico: %.*s, data: %.*s", topic_lenght,
             topic, length, payload);

    char *payload_copy = malloc(sizeof(char) * (length + 1));

    sprintf(payload_copy, "%.*s", length,
            payload); // Copia o payload para a variável payload_copy

    if (strcmp(payload_copy, "update") == 0) {
        char url[140];

        read_value_from_nvs("update_url", (void *)url, STR);

        init_ota(url);
    }

    if (strcmp(payload_copy, "reboot") == 0) { // Se o payload for reboot
        ESP_LOGI(TAG, "Reiniciando");          // Log de reinicialização
        send_message("reiniciando");           // Envia a mensagem
        esp_restart();                         // Reinicia o ESP
    }

    if (strcmp(payload_copy, "ping") == 0) { // Se o payload for shutdown
        ESP_LOGI(TAG, "Pong");               // Log de reinicialização
        send_message("pong");                // Envia a mensagem pong
    }

    free(payload_copy);
}

void dataTask(void *pvParameters) {
    /*
    init_i2c();
    init_bh1750();
    init_bme280();

    bh1750_set_mode(BH1750_CONTINUOS_MODE);
    bme280_set_mode(BME280_FORCE_MODE);
*/

    float temperature = 0.0, humitidy = 0.0, pressure = 0.0, lux = 0.0;

    char payload[128];

    while (1) {
        // read_bh1750(&lux);

        // read_bme280(&pressure, &temperature, &humitidy);

        sprintf(payload,
                "{\"pressure\":%.2f,\"temperature\":%.2f,\"humitidy\":%.2f,"
                "\"lux\":%.2f}",
                pressure, temperature, humitidy, lux);

        send_message(payload);

        ESP_LOGI(TAG, "dados enviados: %s", payload);

        vTaskDelay(60000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}